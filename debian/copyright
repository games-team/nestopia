Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: Nestopia UE
Source: <http://0ldsk00l.ca/nestopia>
Files-Excluded: source/unrar
Comment: unrar is non-free

Files: *
Copyright: 2003-2008 Martin Freij
           2007-2008 R. Belmont
           2012-2024 R. Danbrook
           2020-2024 Rupert Carmichael
           2018 Phil Smith
License: GPL-2.0+

Files: shaders/crtea.fs
       shaders/default.fs
       shaders/default.vs
       shaders/sharp-bilinear.fs
Copyright: 2020-2022 Rupert Carmichael
License: Expat

Files: shaders/mmpx.fs
Copyright: Morgan McGuire and Mara Gagiu
License: Expat

Files: shaders/omniscale.fs
Copyright: Lior Halphon
License: Expat

Files: source/core/NstVideoFilterxBR.cpp
Copyright: 2011, 2012 Hyllian/Jararaca
License: GPL-2.0+

Files: source/fltkui/audiomanager.cpp
       source/fltkui/inputmanager.cpp
       source/fltkui/videomanager.cpp
Copyright: 2012-2024 R. Danbrook
           2020-2024 Rupert Carmichael
License: BSD-3-clause

Files: source/fltkui/chtmanager.cpp
       source/fltkui/fltkui*.cpp
       source/fltkui/jgmanager.cpp
       source/fltkui/logdriver.cpp
       source/fltkui/setmanager.cpp
       source/fltkui/uiadapter.cpp
Copyright: 2012-2024 R. Danbrook
License: BSD-3-clause

Files: source/fltkui/jg*.h
Copyright: 2020-2022 Rupert Carmichael
License: Zlib

Files: source/fltkui/lodepng.*
Copyright: 2005-2023 Lode Vandevenne
License: Zlib

Files: source/nes_ntsc/*
Copyright: 2006-2007 Shay Green
License: LGPL-2.1+

Files: debian/*
Copyright: 2011-2018, 2020-2022, 2024 Stephen Kitt <skitt@debian.org>
License: GPL-2.0+

Files: debian/nestopia.appdata.xml
Copyright: 2017 Jeremy Bicha
License: CC0-1.0


License: Expat
 Permission is hereby granted, free of charge, to any person obtaining
 a copy of this software and associated documentation files (the
 "Software"), to deal in the Software without restriction, including
 without limitation the rights to use, copy, modify, merge, publish,
 distribute, sublicense, and/or sell copies of the Software, and to
 permit persons to whom the Software is furnished to do so, subject to
 the following conditions:
 .
 The above copyright notice and this permission notice shall be
 included in all copies or substantial portions of the Software.
 .
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS
 BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN
 ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
 CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 SOFTWARE.

License: GPL-2.0+
 This package is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or
 (at your option) any later version.
 .
 This package is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 .
 You should have received a copy of the GNU General Public License
 along with this program. If not, see <http://www.gnu.org/licenses/>
 .
 On Debian systems, the complete text of the GNU General
 Public License version 2 can be found in
 "/usr/share/common-licenses/GPL-2".

License: LGPL-2.1+
 This module is free software; you can redistribute it and/or modify
 it under the terms of the GNU Lesser General Public License as
 published by the Free Software Foundation; either version 2.1 of the
 License, or (at your option) any later version.
 .
 This module is distributed in the hope that it will be useful, but
 WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 Lesser General Public License for more details.
 .
 You should have received a copy of the GNU Lesser General Public
 License along with this module; if not, write to the Free Software
 Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 02110-1301 USA.
 .
 On Debian systems, the complete text of the GNU Lesser General Public
 License version 2.1 can be found in
 "/usr/share/common-licenses/LGPL-2.1".

License: Zlib
 This software is provided 'as-is', without any express or implied
 warranty.  In no event will the authors be held liable for any
 damages arising from the use of this software.
 .
 Permission is granted to anyone to use this software for any purpose,
 including commercial applications, and to alter it and redistribute
 it freely, subject to the following restrictions:
 .
 1. The origin of this software must not be misrepresented; you must
    not claim that you wrote the original software. If you use this
    software in a product, an acknowledgment in the product
    documentation would be appreciated but is not required.
 2. Altered source versions must be plainly marked as such, and must
    not be misrepresented as being the original software.
 3. This notice may not be removed or altered from any source
    distribution.

License: BSD-3-clause
 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions
 are met:
 .
 1. Redistributions of source code must retain the above copyright
    notice, this list of conditions and the following disclaimer.
 .
 2. Redistributions in binary form must reproduce the above copyright
    notice, this list of conditions and the following disclaimer in
    the documentation and/or other materials provided with the
    distribution.
 .
 3. Neither the name of the copyright holder nor the names of its
    contributors may be used to endorse or promote products derived
    from this software without specific prior written permission.
 .
 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS
 OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
 AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY
 WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 POSSIBILITY OF SUCH DAMAGE.

License: CC0-1.0
 Creative Commons CC0 1.0 Universal
 .
 CREATIVE COMMONS CORPORATION IS NOT A LAW FIRM AND DOES NOT PROVIDE
 LEGAL SERVICES. DISTRIBUTION OF THIS DOCUMENT DOES NOT CREATE AN
 ATTORNEY-CLIENT RELATIONSHIP. CREATIVE COMMONS PROVIDES THIS INFORMATION
 ON AN "AS-IS" BASIS. CREATIVE COMMONS MAKES NO WARRANTIES REGARDING THE
 USE OF THIS DOCUMENT OR THE INFORMATION OR WORKS PROVIDED HEREUNDER, AND
 DISCLAIMS LIABILITY FOR DAMAGES RESULTING FROM THE USE OF THIS DOCUMENT
 OR THE INFORMATION OR WORKS PROVIDED HEREUNDER.
 .
 Statement of Purpose
 .
 The laws of most jurisdictions throughout the world automatically confer
 exclusive Copyright and Related Rights (defined below) upon the creator
 and subsequent owner(s) (each and all, an "owner") of an original work
 of authorship and/or a database (each, a "Work").
 .
 Certain owners wish to permanently relinquish those rights to a Work for
 the purpose of contributing to a commons of creative, cultural and
 scientific works ("Commons") that the public can reliably and without
 fear of later claims of infringement build upon, modify, incorporate in
 other works, reuse and redistribute as freely as possible in any form
 whatsoever and for any purposes, including without limitation commercial
 purposes. These owners may contribute to the Commons to promote the
 ideal of a free culture and the further production of creative, cultural
 and scientific works, or to gain reputation or greater distribution for
 their Work in part through the use and efforts of others.
 .
 For these and/or other purposes and motivations, and without any
 expectation of additional consideration or compensation, the person
 associating CC0 with a Work (the "Affirmer"), to the extent that he or
 she is an owner of Copyright and Related Rights in the Work, voluntarily
 elects to apply CC0 to the Work and publicly distribute the Work under
 its terms, with knowledge of his or her Copyright and Related Rights in
 the Work and the meaning and intended legal effect of CC0 on those
 rights.
 .
 1. Copyright and Related Rights. A Work made available under CC0 may be
 protected by copyright and related or neighboring rights ("Copyright and
 Related Rights"). Copyright and Related Rights include, but are not
 limited to, the following:
 .
 i. the right to reproduce, adapt, distribute, perform, display,
 communicate, and translate a Work;
 .
 ii. moral rights retained by the original author(s) and/or performer(s);
 .
 iii. publicity and privacy rights pertaining to a person's image or
 likeness depicted in a Work;
 .
 iv. rights protecting against unfair competition in regards to a Work,
 subject to the limitations in paragraph 4(a), below;
 .
 v. rights protecting the extraction, dissemination, use and reuse of
 data in a Work;
 .
 vi. database rights (such as those arising under Directive 96/9/EC of
 the European Parliament and of the Council of 11 March 1996 on the legal
 protection of databases, and under any national implementation thereof,
 including any amended or successor version of such directive); and
 .
 vii. other similar, equivalent or corresponding rights throughout the
 world based on applicable law or treaty, and any national
 implementations thereof.
 .
 2. Waiver. To the greatest extent permitted by, but not in contravention
 of, applicable law, Affirmer hereby overtly, fully, permanently,
 irrevocably and unconditionally waives, abandons, and surrenders all of
 Affirmer's Copyright and Related Rights and associated claims and causes
 of action, whether now known or unknown (including existing as well as
 future claims and causes of action), in the Work (i) in all territories
 worldwide, (ii) for the maximum duration provided by applicable law or
 treaty (including future time extensions), (iii) in any current or
 future medium and for any number of copies, and (iv) for any purpose
 whatsoever, including without limitation commercial, advertising or
 promotional purposes (the "Waiver"). Affirmer makes the Waiver for the
 benefit of each member of the public at large and to the detriment of
 Affirmer's heirs and successors, fully intending that such Waiver shall
 not be subject to revocation, rescission, cancellation, termination, or
 any other legal or equitable action to disrupt the quiet enjoyment of
 the Work by the public as contemplated by Affirmer's express Statement
 of Purpose.
 .
 3. Public License Fallback. Should any part of the Waiver for any reason
 be judged legally invalid or ineffective under applicable law, then the
 Waiver shall be preserved to the maximum extent permitted taking into
 account Affirmer's express Statement of Purpose. In addition, to the
 extent the Waiver is so judged Affirmer hereby grants to each affected
 person a royalty-free, non transferable, non sublicensable, non
 exclusive, irrevocable and unconditional license to exercise Affirmer's
 Copyright and Related Rights in the Work (i) in all territories
 worldwide, (ii) for the maximum duration provided by applicable law or
 treaty (including future time extensions), (iii) in any current or
 future medium and for any number of copies, and (iv) for any purpose
 whatsoever, including without limitation commercial, advertising or
 promotional purposes (the "License"). The License shall be deemed
 effective as of the date CC0 was applied by Affirmer to the Work. Should
 any part of the License for any reason be judged legally invalid or
 ineffective under applicable law, such partial invalidity or
 ineffectiveness shall not invalidate the remainder of the License, and
 in such case Affirmer hereby affirms that he or she will not (i)
 exercise any of his or her remaining Copyright and Related Rights in the
 Work or (ii) assert any associated claims and causes of action with
 respect to the Work, in either case contrary to Affirmer's express
 Statement of Purpose.
 .
 4. Limitations and Disclaimers.
 .
 a. No trademark or patent rights held by Affirmer are waived, abandoned,
 surrendered, licensed or otherwise affected by this document.
 .
 b. Affirmer offers the Work as-is and makes no representations or
 warranties of any kind concerning the Work, express, implied, statutory
 or otherwise, including without limitation warranties of title,
 merchantability, fitness for a particular purpose, non infringement, or
 the absence of latent or other defects, accuracy, or the present or
 absence of errors, whether or not discoverable, all to the greatest
 extent permissible under applicable law.
 .
 c. Affirmer disclaims responsibility for clearing rights of other
 persons that may apply to the Work or any use thereof, including without
 limitation any person's Copyright and Related Rights in the Work.
 Further, Affirmer disclaims responsibility for obtaining any necessary
 consents, permissions or other rights required for any use of the Work.
 .
 d. Affirmer understands and acknowledges that Creative Commons is not a
 party to this document and has no duty or obligation with respect to
 this CC0 or use of the Work.
